% rebase('base.tpl')
<div class="smart-green">
<h1>Uzta is saving the data. It can take a long time, for example half an hour. You will receive an email with the download link when the job is done. You will need a token for the download. Record it well because it will <i>not</i> be in the email (for security reasons)</h1>
<p><b>Token: <h2>{{token}}</h2></b></p>
</div>