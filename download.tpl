% rebase('base.tpl')
<form class="smart-green" action="/file" method="post">
  <h1>Congratulations! The data is saved. Provide the token to
  get the archive:</h1>
  <label for="token">Token: </label>
  <input name="token" id="token" type="text">
  <br><br><br>
  <input value="Get ZIP file" type="submit">
</form>