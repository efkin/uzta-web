# uzta-web
A poor and minimal frontend to [uzta](https://gitlab.com/calafou/uzta) software written with [bottle](http://bottlepy.org/docs/dev/index.html) framework.

## Installation
Tested on Debian (stable and testing):

1. Follow the instructions on our [Contribution Guide](https://gitlab.com/efkin/uzta-web/blob/master/CONTRIBUTING.md) to create a development environment but substitute steps 9 and 10 with:
```bash
pip3 install --upgrade -r requirements/production.txt
```

2. Download `uzta` software from Calafou repo.
```bash
wget https://gitlab.com/calafou/uzta/raw/master/uzta.py
```

3. Run `uzta-web` in production:
```bash
gunicorn uzta-web:app
```
