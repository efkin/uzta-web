#!/usr/bin/python3
from bottle import get, post, abort, request, run, default_app, redirect, template, static_file, route
from email.mime.text import MIMEText
from threading import Thread
from time import sleep
from uuid import uuid4
from uzta import uzta
from uzta import __version__ as uzta_version
import email.utils as eu
import os
import settings
import smtplib
import string


url = settings.production['url']


def thread_it(function):
    def decorator(*args, **kwargs):
        t = Thread(target=function, args=args, kwargs=kwargs)
        # let the thread be killed by the main process.
        t.daemon = True
        t.start()
    return decorator
                                    

def send_mail(msg, email, subject):
    msg = MIMEText(msg)
    msg['To'] = eu.formataddr(('Recipient', email))
    msg['From'] = eu.formataddr(('Uzta Team', settings.production['EMAIL_ADDRESS']))
    msg['Subject'] = "[Uzta] " + subject 

    server = smtplib.SMTP(settings.production['SMTP_SERVER'], 587)
    server.starttls()
    server.login(settings.production['EMAIL_USER'], settings.production['EMAIL_PASSWORD'])

    try:
        server.sendmail(settings.production['EMAIL_ADDRESS'], [email], msg.as_string())
    finally:
        server.quit()


@thread_it
def harvest_and_report(username, password, website, email, token):
    """
    this is an enhanced and threaded version of uzta function.
    it takes care of the error reporting and the final success.
    """
    error = uzta(username, password, website, uzta_version, token + '.tmp', True, token, -1)
    if not error:
        send_mail("Download your archive from %sdownload" % url, email, "Your download is ready")
    else:
        send_mail("Uzta encountered the following errors: %s" % str(error), email, "Ooops! Errors occurred")
    

# Web app

@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='static/')


@get('/')
def index():
    return template('index')


@get('/success/<token>')
def success(token):
    return template('success', token=token)


@post('/scrape')
def scrape():
    username = request.forms.get('username')
    password = request.forms.get('password')
    website = request.forms.get('website')
    email = request.forms.get('email')
    token = str(uuid4())
    harvest_and_report(username, password, website, email, token)
    return redirect('/success/%s' % token)


@get('/download')
def download():
    return template('download')


@post('/file')
def file():
    token = request.forms.get('token')
    root = os.path.join(os.getcwd(), token)
    if os.path.isdir(root):
        return static_file('uzta.zip', root, 'application/zip')
    else:
        return abort(401, 'Token expired or wrong. <a href="%sdownload">retry</a> the token or \
<a href="%s">restart</a> the job.' % (url, url))


if __name__ == '__main__':
    try:
        run(host='localhost', port=8080, debug=settings.development['debug'])
    except Exception as e:
        print(e)
        exit(1)


app = default_app()

