# Settings file
# It stores the settings for the app in
# different dictionaries, one per desired setup.

import os


class ImproperlyConfigured(Exception):
    pass


def get_env_variable(var_name):
    """
    Get the enviroment variable or return an exception
    """
    try:
        return os.environ[var_name]
    except:
        raise ImproperlyConfigured("Please set the %s environment variable" % var_name)


development = {
    'debug': True,
    }

production = {
    'debug': False,
    'url': 'http://localhost:8000/',
    'EMAIL_USER': 'uzta',
    'SMTP_SERVER': 'mail.example.org',
    'EMAIL_ADDRESS': 'uzta@example.org',
    'STARTTLS': True,
    'EMAIL_PASSWORD': get_env_variable("EMAIL_PASSWORD")
    }

